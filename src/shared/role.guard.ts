import {
	Injectable,
	CanActivate,
	ExecutionContext,
	Logger,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RoleGuard implements CanActivate {
	constructor(private readonly reflector: Reflector) {}

	canActivate(
		context: ExecutionContext,
	): boolean | Promise<boolean> | Observable<boolean> {
		const request = context.switchToHttp().getRequest();
		const userRole = request.user.role;
		Logger.log(userRole, 'RoleGuard');
		const requiredRole = this.reflector.get<string>(
			'role',
			context.getHandler(),
		);

		if (!requiredRole) return true;

		return userRole === requiredRole;
	}
}
