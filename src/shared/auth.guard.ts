import {
	Injectable,
	CanActivate,
	ExecutionContext,
	HttpException,
	HttpStatus,
	Logger,
} from '@nestjs/common';

import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthGuard implements CanActivate {
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest();
		if (!request.headers.authorization) {
			return false;
		}
		request.user = await this.validateToken(request.headers.authorization);
		Logger.log(request.user, 'USER');
		return true;
	}

	async validateToken(auth: string) {
		if (auth.split(' ')[0] !== 'Bearer') {
			throw new HttpException('Invalid Token', HttpStatus.FORBIDDEN);
		}

		const token = auth.split(' ')[1];
		try {
			return jwt.verify(token, process.env.TOKEN_SECRET);
		} catch (err) {
			const message = 'Token error: ' + (err.message || err.name);
			throw new HttpException(message, HttpStatus.FORBIDDEN);
		}
	}
}
