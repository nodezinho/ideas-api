import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { IdeaEntity } from './idea.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { IdeaDTO, IdeaResponseObject } from './idea.dto';
import { UserEntity } from 'src/user/user.entity';

@Injectable()
export class IdeaService {
	constructor(
		@InjectRepository(IdeaEntity)
		private ideaRepository: Repository<IdeaEntity>,
		@InjectRepository(UserEntity)
		private userRepository: Repository<UserEntity>,
	) {}

	async showAll(): Promise<IdeaResponseObject[]> {
		const ideas = await this.ideaRepository.find({ relations: ['author'] });
		return ideas.map(idea => this.toResponseObject(idea));
	}

	async create(data: IdeaDTO, userId: string): Promise<IdeaResponseObject> {
		const user = await this.findOneUser(userId);
		const idea = this.ideaRepository.create({ ...data, author: user });
		await this.ideaRepository.save(idea);
		return this.toResponseObject(idea);
	}

	async readOne(id: string): Promise<IdeaResponseObject> {
		const idea = await this.findOneIdea(id);
		return this.toResponseObject(idea);
	}
	//Partial to expect partial IdeaDTO, ex: {description: 'dasd'}, not entirely object
	async update(
		id: string,
		data: Partial<IdeaDTO>,
		userId: string,
	): Promise<IdeaResponseObject> {
		const idea = await this.findOneIdea(id);
		this.ensureOwnership(idea, userId);
		await this.ideaRepository.update({ id }, data);
		return this.toResponseObject(idea);
	}

	async destroy(id: string, userId: string) {
		const idea = await this.findOneIdea(id);
		this.ensureOwnership(idea, userId);
		await this.ideaRepository.delete({ id });
		return this.toResponseObject(idea);
	}

	private async findOneIdea(id: string) {
		const idea = await this.ideaRepository.findOne({
			where: { id: id },
			relations: ['author'],
		});
		if (!idea) {
			throw new HttpException('Not found', HttpStatus.NOT_FOUND);
		}
		return idea;
	}

	private async findOneUser(userId: string) {
		return this.userRepository.findOne({ where: { id: userId } });
	}

	private toResponseObject(idea: IdeaEntity) {
		Logger.log(idea, 'IdeaService');
		return { ...idea, author: idea.author.toResponseObject(false) };
	}

	private ensureOwnership(idea: IdeaEntity, userId: string) {
		if (idea.author.id !== userId) {
			throw new HttpException(
				'Incorrect owner of data',
				HttpStatus.UNAUTHORIZED,
			);
		}
	}
}
