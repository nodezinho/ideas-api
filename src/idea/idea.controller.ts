import {
	Controller,
	Get,
	Post,
	Put,
	Delete,
	Body,
	Param,
	UsePipes,
	UseGuards,
	Logger,
} from '@nestjs/common';
import { IdeaService } from './idea.service';
import { IdeaDTO } from './idea.dto';
import { ValidationPipe } from 'src/shared/validation.pipe';
import { AuthGuard } from 'src/shared/auth.guard';
import { UserRef } from 'src/user/user.decorator';

@Controller('idea')
export class IdeaController {
	constructor(private ideaService: IdeaService) {}

	@Get()
	showAllIdeas() {
		return this.ideaService.showAll();
	}

	@Post()
	@UseGuards(new AuthGuard())
	@UsePipes(new ValidationPipe())
	createIdea(@Body() data: IdeaDTO, @UserRef('id') userId) {
		Logger.log(userId, 'IdeaController');
		return this.ideaService.create(data, userId);
	}

	@Get(':id')
	readOneIdea(@Param('id') id: string) {
		return this.ideaService.readOne(id);
	}

	@Put(':id')
	@UseGuards(new AuthGuard())
	@UsePipes(new ValidationPipe())
	updateIdea(
		@Param('id') id: string,
		@Body() data: Partial<IdeaDTO>,
		@UserRef('id') userId: string,
	) {
		return this.ideaService.update(id, data, userId);
	}

	@Delete(':id')
	@UseGuards(new AuthGuard())
	destroyIdea(@Param('id') id: string, @UserRef('id') userId: string) {
		Logger.log(userId, 'IdeaController');
		return this.ideaService.destroy(id, userId);
	}
}
