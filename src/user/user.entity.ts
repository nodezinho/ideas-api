import {
	Entity,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	Column,
	BeforeInsert,
	OneToMany,
} from 'typeorm';

import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { IdeaEntity } from 'src/idea/idea.entity';

@Entity('user')
export class UserEntity {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@CreateDateColumn()
	created: Date;

	@Column({
		type: 'text',
		unique: true,
	})
	username: string;

	@Column('text')
	password: string;

	@OneToMany(
		type => IdeaEntity,
		idea => idea.author,
	)
	ideas: IdeaEntity[];

	@BeforeInsert()
	async hashPassword() {
		this.password = await bcrypt.hash(this.password, 10);
	}

	toResponseObject(showToken: boolean = true): UserResponseObject {
		const { id, created, username, token } = this;
		const responseObject: UserResponseObject = {
			id,
			username,
			created,
		};
		if (showToken) {
			responseObject.token = token;
		}
		if (this.ideas) {
			responseObject.ideas = this.ideas;
		}
		return responseObject;
	}

	async comparePassword(pass: string) {
		return await bcrypt.compare(pass, this.password);
	}

	private get token() {
		const { id, username } = this;
		return jwt.sign(
			{
				id,
				username,
			},
			process.env.TOKEN_SECRET,
			{ expiresIn: '1d' },
		);
	}
}

export interface UserResponseObject {
	id: string;
	username: string;
	created: Date;
	ideas?: IdeaEntity[];
	token?: string;
}
