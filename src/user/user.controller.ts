import {
	Controller,
	Get,
	Post,
	Body,
	UsePipes,
	UseGuards,
	Logger,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ValidationPipe } from 'src/shared/validation.pipe';
import { UserDTO } from './user.dto';
import { AuthGuard } from 'src/shared/auth.guard';
import { UserRef } from './user.decorator';
import { RoleGuard } from 'src/shared/role.guard';
import { Role } from 'src/shared/role.decorator';
import { UserSignupDTO } from './userSingup.dto';

@Controller('api')
export class UserController {
	constructor(private userService: UserService) {}

	@Get('users')
	@Role('admin')
	@UseGuards(new AuthGuard())
	showAllUsers(@UserRef('role') userRef) {
		return this.userService.showAll();
	}

	@Post('signin')
	@UsePipes(new ValidationPipe())
	signin(@Body() data: UserDTO) {
		return this.userService.signin(data);
	}

	@Post('signup')
	@UsePipes(new ValidationPipe())
	signup(@Body() data: UserDTO) {
		return this.userService.signup(data);
	}
}
