import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { Repository } from 'typeorm';
import { UserDTO } from './user.dto';

@Injectable()
export class UserService {
	constructor(
		@InjectRepository(UserEntity)
		private userRepository: Repository<UserEntity>,
	) {}

	async showAll() {
		const users = await this.userRepository.find({
			relations: ['ideas'],
		});
		return users.map(user => user.toResponseObject(false));
	}

	async signin(data: UserDTO) {
		const user = await this.findUser(data);
		if (!user || !(await user.comparePassword(data.password))) {
			throw new HttpException(
				'Invalid username / password',
				HttpStatus.BAD_REQUEST,
			);
		}
		return user.toResponseObject();
	}

	async signup(data: UserDTO) {
		let user = await this.findUser(data);
		if (user) {
			throw new HttpException(
				'User Already exists',
				HttpStatus.BAD_REQUEST,
			);
		}
		user = this.userRepository.create(data);
		await this.userRepository.save(user);
		return user.toResponseObject();
	}

	private async findUser(data: UserDTO) {
		const { username } = data;
		return this.userRepository.findOne({ where: { username } });
	}
}
