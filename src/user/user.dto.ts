import { IsNotEmpty, MIN_LENGTH, Length } from 'class-validator';

export class UserDTO {
	@IsNotEmpty()
	username: string;

	@IsNotEmpty()
	@Length(6)
	password: string;
}
