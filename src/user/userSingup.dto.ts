import { IsNotEmpty, MIN_LENGTH, Length, IsOptional } from 'class-validator';

export class UserSignupDTO {
	@IsNotEmpty()
	username: string;

	@IsNotEmpty()
	@Length(6)
	password: string;
}
