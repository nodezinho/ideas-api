import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

const port = process.env.API_PORT || 8080;

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	await app.listen(port, () => {
		Logger.log(`Api running on ${port}`, 'Main');
	});
}
bootstrap();
