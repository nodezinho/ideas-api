## Description

Nest API + postgres db

## Running the app

```bash
# watch mode
$ docker-compose up

```

#### commands to interact with docker container and postgres

```bash
# enter container
$ docker exec -i -t ${container_id} /bin/bash

# interact with postgres
$ psql --help



```
